<?php

return [
    'actions' => [
        'delete' => 'Rekord usunięto',
        'add' => 'Rekord dodano',
        'update' => 'Rekord zaktualizowano',

    ],
];
