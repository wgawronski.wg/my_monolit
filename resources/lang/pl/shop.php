<?php

return [
    'price' => [
        'shortcut' => 'PLN',
        'name' => 'Cena',
    ],
    'order' => [
        'total' => 'Całość zamówienia'
    ],
];
