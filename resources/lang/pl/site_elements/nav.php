<?php

return [
    'start' => 'Start',
    'products' => 'Produkty',
    'api' => [
        'title' => 'Publiczne API',
        'product' => 'Produkt',
        'products' => 'Produkty',
        'pagination' => 'Produkty: Paginacja'
    ],
    'contact' => 'Kontakt',
    'telescope' => 'Telescope',
];
