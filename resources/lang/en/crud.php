<?php

return [
    'actions' => [
        'delete' => 'Delete record',
        'add' => 'Add record',
        'update' => 'Record updated',
    ],
];
