<?php

return [
    'price' => [
        'shortcut' => 'EUR',
    ],
    'order' => [
        'total' => 'Order total'
    ],
];
