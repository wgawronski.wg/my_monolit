@extends('layout.my_monolit')

@section('title', 'Products')

@section('main')
    <section id="categories">
        <div class="card pt-5 mb-4">
            <div class="card-body">
                <div class="mb-5">
                    <h1 class="display-1 text-center">{{ $category->name }}</h1>
                    <p class="lead text-center">See our products</p>
                </div>
                <ul class="list-group lead text-center">
                    @forelse($category->products as $item)
{{--                        {{ dd($item) }}--}}
                        <li class="list-group-item p-0" aria-current="true">
                            <a class="d-block py-3" href="{{ route('web.products.show', ['slug' => $item->slug]) }}">{{ $item->name }}</a>
                        </li>
                    @empty
                        <p>No categosies</p>
                    @endforelse
                </ul>
            </div>
        </div>
    </section>
@endsection
