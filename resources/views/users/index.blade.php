{{--https://bbbootstrap.com/snippets/ecommerce-shopping-cart-30657531--}}

@extends('layout.my_monolit')

@section('title', 'Users')

@section('main')
    @include('layout.my_monolit.site_elements.flash_message')
    <section id="cart">
        <div class="card pt-5 mb-4">
            <div class="card-body">
                <div class="mb-5">
                    <h1 class="display-1 text-center">Users</h1>
                    <p class="lead text-center">Let's start</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <table class="table table-striped table-sm small">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">ID</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Created</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($users as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <th scope="row">{{ $item->id }}</th>
                        <td>{{ $item->name }}</td>
                        <td><a href="mailto:{{ $item->email }}">{{ $item->email }}</a></td>
                        <td>{{ $item->created_at }}</td>
                        <td>
                            <a class="btn btn-secondary btn-sm" href="{{ route('web.users.edit', ['user' => $item->id]) }}">
                                E
                            </a>
                        </td>
                    </tr>
                    @empty
                        No Users
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@endsection
