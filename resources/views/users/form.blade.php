<div class="mb-3">
    <label for="name" class="form-label">Name:</label>
    <input type="text" class="form-control" id="name" name="name" disabled placeholder="User name" value="{{ old('name') ?? $user->name ?? '' }}">
    <div id="nameHelp" class="form-text small">Required field</div>
</div>
<div class="mb-3">
    <label for="email" class="form-label">Email:</label>
    <input type="email" class="form-control" id="email" disabled name="email" placeholder="User email" value="{{ old('email') ?? $user->email ?? '' }}">
    <div id="emailHelp" class="form-text small">Required field</div>
</div>

<div class="mb-3 row">
    <label for="inputCity" class="col-sm-2 col-form-label">City</label>
    <div class="col-sm-10">
        <input name="address[city]" value="{{ old('address["city"]') ?? $user->address->city ?? '' }}" type="text" class="form-control" id="inputCity">
    </div>
</div>

<div class="mb-4 row">
    <label for="inputStreet" class="col-sm-2 col-form-label">Street</label>
    <div class="col-sm-10">
        <input name="address[street]" value="{{ old('street') ?? $user?->address?->street }}" type="text" class="form-control" id="inputStreet">
    </div>
</div>

<div class="row mb-4">
    <div class="col-md-4">
        <label for="street-no" class="form-label small text-muted mb-0">Street no.:</label>
        <input type="number" name="address[street_no]" class="form-control" id="street-no" value="{{ old('street_no') ?? $user->address->street_no ?? '' }}" >
    </div>
    <div class="col-md-4">
        <label for="home-no" class="form-label small text-muted mb-0">Home no.:</label>
        <input type="number" name="address[home_no]" class="form-control" id="home-no" value="{{ old('home_no') ?? $user->address->home_no ?? '' }}" >
        <div class="valid-feedback">
            Looks good!
        </div>
    </div>
    <div class="col-md-4">
        <label for="zip-code" class="form-label small text-muted mb-0">Zip Code</label>
        <div class="input-group has-validation">
            <input type="number" name="address[zip_code]" class="form-control" id="zip-code" value="{{ old('zip_code') ?? $user->address->zip_code ?? '' }}" >
        </div>
    </div>
</div>
