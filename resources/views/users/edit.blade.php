@extends('layout.my_monolit')

@section('title', 'Users')

@section('main')
    <div class="card  pt-5 mb-4">
        <div class="card-body">
            <h1 class="display-1 text-center">Edit a User</h1>
            <p class="text-center lead my-4">***</p>
            @include('layout.my_monolit.site_elements.flash_message')
            @include('layout.my_monolit.site_elements.form_errors')
            <form action="{{ route('web.users.update', ['user' => $user->id]) }}" method="post">
                @method('PATCH')
                @csrf
                @include('users.form')
                <input type="submit" class="btn btn-success" value="Update user">
            </form>
        </div>
    </div>
@endsection
