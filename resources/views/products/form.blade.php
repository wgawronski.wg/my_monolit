<div class="mb-3">
    <label for="name" class="form-label">Product name</label>
    <input type="text" class="form-control" id="name" name="name" placeholder="Product name" value="{{ old('name') ?? $product->name ?? '' }}">
    <div id="nameHelp" class="form-text small">Required field</div>
</div>
<div class="mb-3">
    <label for="description" class="form-label">{{ __('products.form.label.description') }}</label>
    <textarea class="form-control" name="description" id="description" rows="3" placeholder="...">{{ old('description') ?? $product->description ?? '' }}</textarea>
    <div id="descriptionHelp" class="form-text small">Required field</div>
</div>
<div class="mb-3">
    <label for="price" class="form-label">Price</label>
    <input type="number" name="price" class="form-control" id="price" placeholder="Price" value="{{ old('price') ?? $product->price ?? '' }}">
    <div id="numberHelp" class="form-text small">Required field</div>
</div>
