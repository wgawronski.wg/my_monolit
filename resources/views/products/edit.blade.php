@extends('layout.my_monolit')

@section('title', 'Products')

@section('main')
    <div class="card  pt-5 mb-4">
        <div class="card-body">
            <h1 class="display-1 text-center">Edit a product</h1>
            <p class="text-center lead my-4">***</p>
            @include('layout.my_monolit.site_elements.form_errors')
            <form action="{{ route('web.products.update', ['product' => $product->id]) }}" method="post">
{{--                @method('PUT')--}}
                @method('PATCH')
                @csrf
                @include('products.form')
                <input type="submit" class="btn btn-success" value="Update product">
            </form>
            <form action="{{ route('web.products.destroy', ['product' => $product->id]) }}" method="POST">
                @csrf
                @method('DELETE')
                <input type="submit" class="btn btn-danger mt-2" value="Delete product">
            </form>
        </div>
    </div>
@endsection
