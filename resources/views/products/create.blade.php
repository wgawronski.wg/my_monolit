@extends('layout.my_monolit')

@section('title', 'Products')

@section('main')
    <div class="card  pt-5 mb-4">
        <div class="card-body">
            <h1 class="display-1 text-center">Create a product</h1>
            <p class="text-center lead my-4">***</p>
            @include('layout.my_monolit.site_elements.form_errors')
            <form action="{{ route('web.products.store') }}" method="post">
                @method('POST')
                @csrf
                @include('products.form')
                <input type="submit" class="btn btn-success" value="Add product">
            </form>
        </div>
    </div>
@endsection
