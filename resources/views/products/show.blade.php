@extends('layout.my_monolit')
@section('main')
    @include('layout.my_monolit.site_elements.flash_message')
    <div class="card text-center py-5 mb-4">
        <div class="card-body">
            <h1 class="display-1">{{ $product->name }}</h1>
            <p class="lead">{{ $product->description }}</p>
            <p class="py-3" class="text-muted">
                {{ __('shop.price.name') }} {{ $product->price }} {{ __('shop.price.shortcut') }}
            </p>
            <div class="py-3">
                @include('layout.my_monolit.site_elements.link_btn_dark', ['href' => url()->previous(), 'text' => 'Previous'])
                <a class="btn btn-dark" href="{{ url()->previous() }}">Previous</a>
                @include('layout.my_monolit.site_elements.link_btn_dark', ['href' => route('web.products.edit', ['product' => $product->slug]), 'text' => 'Edit'])
            </div>
            <div>
                <a class="btn btn-outline-dark px-5" href="{{ route('web.categories.show', ['slug' => $product->category->slug]) }}">{{ $product->category->name }}</a>
                <div class="mt-3">
                    <form action="{{ route('web.cart.store', ['product' => $product->id]) }}" method="POST">
                        @csrf
                        @method('POST')
                        <input type="submit" class="btn btn-outline-info" value="Add to chart">
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row mb-3">
        <div class="col col-lg-8">
            <article>
                <h3 class="display-6 mt-4 mb-2">Add a comment:</h3>
                @include('layout.my_monolit.site_elements.form_errors')
                <form action="{{ route('web.products.comments.store', ['product' => $product->id]) }}" method="POST">
                    @csrf
                    @method('POST')
                    @include('products.comments.form')
                    <input type="submit" class="btn btn-outline-dark" value="Add">
                </form>
            </article>
            <div>
                <hr class="my-4">
                <h4 class="lead mb-3">Comments: {{ $product->comments->count() }}</h4>
                @forelse($product->comments as $comment)
                    @include('products.comments.comment')
                @empty
                    <p>No comments!</p>
                @endforelse
            </div>
        </div>
    </div>
@endsection
