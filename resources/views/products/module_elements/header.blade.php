<div class="bg-dark p-1 mb-1 text-white">
    <form id="myForm" method="get" onsubmit="return submitForm()">
        <div class="d-flex justify-content-between">
            <select id="select-action" class="form-select" id="select-action" aria-label="Default select example">
                <option selected>Select a product</option>
                @forelse($products as $item)
                    <option value="{{ route('web.products.comments.index', ['product' => $item->slug]) }}">{{ $item->name }}</option>
                @empty
                    -
                @endforelse
            </select>
            <input type="submit" value="Go" class="btn btn-info px-5 ms-2">
        </div>
    </form>
</div>

@section('js')
    <script type="text/javascript">
        function submitForm() {
            var url = $('#select-action').val();
            $('#myForm').attr('action', url);
            $('form#myForm').submit();
            return false;
        }
    </script>
@endsection
