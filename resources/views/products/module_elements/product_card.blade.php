<div class="card text-center py-5 mb-4">
    <div class="card-body">
        <h1 class="display-1">{{ $product->name }}</h1>
        <p class="lead">{{ $product->description }}</p>
        <p class="py-3" class="text-muted">
            {{ __('shop.price.name') }} {{ $product->price }} {{ __('shop.price.shortcut') }}
        </p>
    </div>
</div>
