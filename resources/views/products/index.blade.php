@extends('layout.my_monolit')

@section('title', 'Products')

@section('main')
    @include('layout.my_monolit.site_elements.flash_message')
<section id="products">
    <div class="card pt-5 mb-4">
        <div class="card-body">
            <div class="mb-5">
                <h1 class="display-1 text-center">Our products</h1>
                 <p class="lead text-center">See our prducts</p>
            </div>

            <div class="row">
                    @forelse($products as $item)
                    <div class="col-md-4">
                        <div class="card mb-3">
                            <div class="card-body text-center">
                                <h4><a class="text-decoration-none" href="{{ route('web.products.show', ['slug' => $item->slug]) }}">{{ $item->name }}</a></h4>
                                <p>****</p>
                                <p>price: {{ $item->price }} EUR</p>
                            </div>
                            <div class="card-footer small d-flex justify-content-between">
                                <div>
                                    <a class="btn btn-sm btn-outline-primary" href="{{ route('web.products.show', ['slug' => $item->slug]) }}">Show</a>
                                </div>
                                <div class="d-flex">
                                    <div class="me-2">
                                        <form class="d-inline-block" action="{{ route('web.cart.store', ['product' => $item->id, 'goBackWithMessage' => true]) }}" method="post">
                                            @csrf
                                            @method('POST')
                                            <button type="submit" class="btn btn-sm btn-outline-info"><i class="fas fa-shopping-cart"></i></button>
                                        </form>
                                        <a class="btn btn-sm btn-outline-info" href="{{ route('web.products.edit', ['product' => $item->slug]) }}"><i class="far fa-edit"></i></a>
                                    </div>
                                    <div>
                                        <form action="{{ route('web.products.destroy', ['product' => $item->id]) }}" method="POST">
                                            @method('DELETE')
                                            @csrf
{{--                                            <input type="submit" class="btn btn-sm btn-outline-danger" value="Delete">--}}
                                            <button type="submit" class="btn btn-sm btn-outline-danger"><i class="far fa-trash-alt"></i></button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @empty
                        <p class="lead">No products in this category!</p>
                    @endforelse
            </div>

            <div class="my-3">
                {{ $products->links() }}
            </div>
        </div>
    </div>
</section>
@endsection
