@extends('layout.my_monolit')

@section('title', 'Products')

@section('main')
    @include('layout.my_monolit.site_elements.flash_message')
    <section id="products">
        @include('products.module_elements.header')
    </section>
@endsection
