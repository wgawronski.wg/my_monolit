@extends('layout.my_monolit')

@section('title', 'Products')

@section('main')

    @include('products.module_elements.header')
    @include('products.module_elements.product_card')
    <div class="bg-light p-4 mb-3 border">
    <div class="d-flex justify-content-between small text-muted mb-1">
        <p>{{ $comment->name }} | <a href="mailto:{{ $comment->email }}">{{ $comment->email }}</a> </p>
        <p>{{ $comment->created_at }}</p>
    </div>
    <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis, facilis.</p>
    <div class="my-2 text-end">
        <form class="d-inline-block" action="{{ route('web.products.comments.destroy', ['product' => $product->id, 'comment' => $comment->id]) }}" method="POST">
            @csrf
            @method('DELETE')
            <input type="submit" class="btn btn-sm btn-dark" value="Delete Comment">
        </form>
        <a href="{{ route('web.products.comments.edit', ['product' => $product->id, 'comment' => $comment->id]) }}" class="btn btn-sm btn-outline-dark">Edit Comment</a>
    </div>
</div>
@endsection
