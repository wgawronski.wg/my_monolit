<div class="row g-3 mb-2">
    <div class="col">
        <input type="text" name="name" class="form-control mb-0 @error('name') is-invalid @enderror" placeholder="Name" aria-label="Name" value="{{ $comment->name ?? old('name') ?? '' }}">
        <div id="nameHelp" class="form-text mt-0 text-muted">* Required, max 32 chars.</div>
        @error('name')
        <div class="alert alert-danger small mt-1">{{ $message }}</div>
        @enderror
    </div>
    <div class="col">
        <input type="text" name="comment-email" class="form-control @error('comment-email') is-invalid @enderror" placeholder="Email" aria-label="Email" value="{{ $comment->email ?? old('comment-email') ?? '' }}">
        <div id="emailHelp" class="form-text small mt-0 text-muted">* Required, max 32 chars.</div>
        @error('comment-email')
        <div class="alert alert-danger small mt-1">{{ $message }}</div>
        @enderror
    </div>
</div>
<div class="mb-2">
    <textarea name="comment-content" class="form-control @error('comments-content') is-invalid @enderror" rows="3" placeholder="Your Comment">{{ $comment->content ?? old('comments-content') ?? '' }}</textarea>
    <div id="contentHelp" class="form-text mt-0 text-muted">* Required</div>
    @error('comments-content')
    <div class="alert alert-danger small mt-1">{{ $message }}</div>
    @enderror
</div>
