@extends('layout.my_monolit')

@section('title', 'Products')

@section('main')

    @include('products.module_elements.header')

    <div class="card pt-5 mb-4">
        <div class="card-body">
    <article>
        <div class="text-center mb-5">
            <h1 class="display-1">{{ $product->name }}</h1>
            <h3 class="display-6 ">Add a comment:</h3>
        </div>
        @include('layout.my_monolit.site_elements.form_errors')
        <form action="{{ route('web.products.comments.store', ['product' => $product->id]) }}" method="POST">
            @csrf
            @method('POST')
            @include('products.comments.form')
            <input type="submit" class="btn btn-outline-dark" value="Add">
        </form>
    </article>
        </div>
    </div>
@endsection
