@extends('layout.my_monolit')

@section('main')
    @include('layout.my_monolit.site_elements.flash_message')
    <div class="card  pt-5 mb-4">
        <div class="card-body">
            <h1 class="display-1 text-center mb-4">Edit a comment</h1>
            <form action="{{ route('web.products.comments.update', ['product' => $product->id, 'comment' => $comment->id]),  }}" method="POST">
                @csrf
                @method('PUT')
                @include('products.comments.form')
                <input type="submit" value="Update" class="btn btn-secondary">
            </form>
            <div class="mt-4">
                @include('layout.my_monolit.site_elements.btn_form_delete', ['action' => route('web.products.comments.destroy', ['product' => $product, 'comment' => $comment]), 'description' => 'Delete comment', 'form_class' => 'd-inline'])
                @include('layout.my_monolit.site_elements.link_btn_dark', ['href' => url()->previous(), 'text' => 'Back'])
                @include('layout.my_monolit.site_elements.link_btn_dark', ['href' =>route('web.products.index'), 'text' => 'All products'])
            </div>
        </div>
    </div>
@endsection
