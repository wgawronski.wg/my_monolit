@extends('layout.my_monolit')

@section('title', 'Products')

@section('main')
    @include('layout.my_monolit.site_elements.flash_message')
<section id="products">
    @include('products.module_elements.header')
    <div class="card pt-5 mb-4">
        <div class="card-body">
            <div class="mb-5 text-center">
                <h1 class="display-1">{{ $product->name }}</h1>
                <a href="{{ route('web.products.comments.create', ['product' => $product->slug]) }}" class="mt-3 btn btn-outline-dark rounded-0">Add a comment</a>
            </div>

            <div class="row px-2">
                <table class="table table-striped table-sm">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">ID</th>
                        <th scope="col">Text</th>
                        <th scope="col">Author</th>
                        <th scope="col">Email</th>
                        <th scope="col">Created at</th>
                        <th scope="col">Actions</th>
                    </tr>
                    </thead>
                    <tbody class="small">
                        @forelse($product->comments as $comment)
                            <tr>
                                <th scope="row">{{ $loop->iteration }}</th>
                                <td>{{ $comment->id }}</td>
                                <td>{{ Str::limit($comment->content, 20) }}</td>
                                <td>{{ $comment->name }}</td>
                                <td>{{ $comment->email }}</td>
                                <td>{{ $comment->created_at }}</td>
                                <td>
                                    <a href="{{ route('web.products.comments.edit', ['product' => $product->id, 'comment' => $comment->id]) }}" title="edit comment" class="btn btn-sm btn-outline-primary rounded-0"><i class="far fa-edit"></i></a>
                                    <a href="{{ route('web.products.comments.show', ['product' => $product->slug, 'comment' => $comment->id]) }}" title="show comment" class="btn btn-sm btn-outline-success rounded-0"><i class="far fa-arrow-alt-circle-right"></i></a>
                                    <form class="d-inline-block" action="{{ route('web.products.comments.destroy', ['product' => $product->id, 'comment' => $comment->id]) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-sm btn-outline-danger rounded-0"><i class="far fa-trash-alt"></i></button>
                                    </form>
                                </td>

                            </tr>
                        @empty
                            <p>No comments yet!</p>
                        @endforelse

                    </tbody>
                </table>
            </div>

            <div class="my-3">
{{--                {{ $product->$commenlinks() }}--}}
            </div>
        </div>
    </div>
</section>
@endsection
