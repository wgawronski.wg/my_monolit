<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/darktheme.css') }}">
    <link rel="stylesheet" href="{{ asset('css/my_monolit.css') }}">

</head>
<body>
{{--@include('layout.my_monolit.nav')--}}

<div class="container">
    <div class="row">
        <div class="col-sm-9">
            <div class="card">
                <div class="card-body">
                    <h1>404</h1>
                    <p>HTTP 404 lub Not Found</p>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            @include('layout.my_monolit.aside')
        </div>
    </div>

</div>
@include('layout.my_monolit.footer')
</body>
</html>
