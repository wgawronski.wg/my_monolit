<div class="container mb-2">
    <nav class="navbar navbar-expand-md navbar-light bg-light">
        <div class="container-fluid">
            <a class="navbar-brand" href="{{ route('start_view') }}">
{{--                 {{ env('APP_NAME') }} || php artisan config:cache--}}
                {{ config('app.name') }}
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav me-auto">
                    <li class="nav-item">
                        <a class="nav-link  @if (Request::route()->getName() == 'start_view') active  @endif" aria-current="page" href="{{ route('start_view') }}">
                            {{ __('site_elements/nav.start') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link @if (Request::route()->getName() == 'web.products.index') active  @endif" href="{{ route('web.products.index') }}">
                            {{ __('site_elements/nav.products') }}
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            {{ __('site_elements/nav.api.title') }}
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('api.products.pagination') }}/2">
                                    {{ __('site_elements/nav.api.pagination') }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('api.products.index') }}">
                                    {{ __('site_elements/nav.api.products') }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('api.products.show', ['product' => 10]) }}">
                                    {{ __('site_elements/nav.api.product') }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('api.cart.index' ) }}">
                                    {{ __('site_elements/nav.api.cart.index') }}
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link @if (Request::route()->getName() == 'contact_view') active  @endif" href="{{ route('contact_view') }}">
                            {{ __('site_elements/nav.contact') }}

                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('start_view') }}/telescope">
                            {{ __('site_elements/nav.telescope') }}
                        </a>
                    </li>
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('web.cart.index') }}">
                            <i class="fas fa-shopping-cart"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('web.dark_theme') }}">
                            @if (session('isDark'))
                                <i class="far fa-sun small"></i>
                            @else
                                <i class="far fa-moon small"></i>
                            @endif
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            {{ Config::get('languages')[App::getLocale()] }}
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                            @foreach (Config::get('languages') as $lang => $language)
                                @if ($lang != App::getLocale())
                                    <li class="nav-item">
                                        <a class="dropdown-item" href="{{ route('web.lang_switch', $lang) }}"> {{$language}}</a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>


