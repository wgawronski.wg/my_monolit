@if ($errors->any())
    <div class="alert alert-danger rounded-0 border border-danger small">
        <ul class="mb-0">
            @foreach ($errors->all() as $error)
                @if ($error == 'slug' && count($errors->all()) == 1)
                    <li>{{ $error }} </li>
                @endif
                @if ($error == 'slug') @continue  @endif
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
