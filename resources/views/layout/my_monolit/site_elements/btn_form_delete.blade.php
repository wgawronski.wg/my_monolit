<form action="{{ $action }}" method="POST" class="{{ $form_class ?? '' }}">
    @csrf
    @method('DELETE')
    <input type="submit" class="btn btn-danger {{ $input_class ?? '' }}" value="{{ $description }}">
</form>
