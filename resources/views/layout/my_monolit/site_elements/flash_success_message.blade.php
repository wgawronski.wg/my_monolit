@if ($message = Session::get('success'))
    <div class="alert alert-success alert-dismissible fade show fw-light rounded-0 border border-success" role="alert">
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        <strong>{{ $message }}</strong>
    </div>
@endif
