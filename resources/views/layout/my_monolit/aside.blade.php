<aside class="text-center">
    <div class="mb-2 p-3">
        <p class="small text-muted">TODO: Emails: Events and Listeners</p>
    </div>
    <a class="text-decoration-none" href="{{ route('web.products.start')  }}">
        <div class="mb-2 p-3">
            <p class="lead">{{ __('aside.comments') }}</p>
        </div>
    </a>
    <a class="text-decoration-none" href="{{ route('web.users.index')  }}">
        <div class="mb-2 p-3">
            <p class="lead">{{ __('aside.users') }}</p>
        </div>
    </a>
    <a class="text-decoration-none" href="{{ route('web.orders.index')  }}">
        <div class="mb-2 p-3">
            <p class="lead">{{ __('aside.orders') }}</p>
        </div>
    </a>
    <a class="text-decoration-none" href="{{ route('web.products.create') }}">
        <div class="mb-2 p-3">
            <p class="lead">{{ __('aside.product') }}</p>
        </div>
    </a>

    <div class="p-2 mb-2 categories">
        <p class="lead mb-2"><a href="{{ route('web.categories.index') }}">Categories</a></p>

{{--        @foreach($categories as $item)--}}
{{--        @endforeach--}}
{{--        {{ dd($category->name) }}--}}
{{--        {{ Request::route()->getName() }}--}}
{{--        {{ request()->input('category') }}--}}
{{--            {{ $product->category_id }}--}}
        <ul class="list-group">
            @forelse($categories as $item)
                <li class="list-group-item p-0
                     @isset($category->name) @if($category->name == $item->name) active @endif @endisset
                     @isset($product) @if($product->category_id == $item->id) active @endif @endisset
                    " aria-current="true"
                >
                    <a class="d-block py-2
                        @isset($category->name) @if($category->name == $item->name) text-white @endif @endisset
                        @isset($product) @if($product->category_id == $item->id) text-white @endif @endisset
                        " href="{{ route('web.categories.show', ['slug' => $item->slug]) }}">{{ $item->name }}
                    </a>
                </li>
            @empty
                <p>No categosies</p>
            @endforelse
        </ul>
    </div>

    <div class="p-2 mb-2">
        <p class="lead">Small improvment</p>
        <hr>
        <ol class="small text-muted text-start">
            <li>Font awesome we wlasnych stylach</li>
            <li>jQery z resources</li>
            <li>Ajax? - csrf vs csrf z headera?</li>
        </ol>
    </div>

    <div class="p-2 mb-2">
        <p class="lead">Bigger improvement</p>
        <hr>
        <ol class="small text-muted text-start">
           <li>Sqlite to mysql: Database agnostic</li>
           <li>Docker</li>
           <li>Swagger</li>
            <li>CICD - github actions</li>
        </ol>
    </div>

</aside>
