<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/darktheme.css') }}">
    <link rel="stylesheet" href="{{ asset('css/my_monolit.css') }}">
    @yield('css-files')

    <title>{{ config('app.name') }} @yield('title')</title>
</head>
<body class="@if (session('isDark')) dark-theme @endif">
@include('layout.my_monolit.nav')
<div class="container">
    <div class="row">
{{--            <p>Categories</p>--}}
            <div class="col-md-9">
                <main>
                    @yield('main')
                </main>
            </div>
        <div class="col-md-3">
            @include('layout.my_monolit.aside')
        </div>
    </div>
</div>
@include('layout.my_monolit.footer')

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <script src="{{ asset('js/js.js') }}"></script>
    @yield('js')
</body>
</html>
