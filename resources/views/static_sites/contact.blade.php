@extends('layout.my_monolit')

@section('main')
    <div class="card py-5 mb-4">
        <div class="card-body">
            <div class="text-center">
                <h1 class="display-1">Contact</h1>
                <p class="lead mb-3">Contact us. Write an email or fill in the form.</p>
                <p class="lead"><a href="">shop@contact.us</a></p>
            </div>
            <section class="p-5 pb-2 pt-4">
                @include('layout.my_monolit.site_elements.flash_success_message')
                <form action="{{ route('web.contact_form') }}" method="POST">
                    @csrf
                    @method('POST')
                    <div class="row g-3 mb-2">
                        <div class="col-lg-6">
                            <label for="name" class="small text-muted">Name:</label>
                            <input type="text" name="name" id="name" class="form-control mb-0 @error('name') is-invalid @enderror" placeholder="Name" aria-label="Name" value="{{ $comment->name ?? old('name') ?? '' }}">
                            <div id="nameHelp" class="form-text mt-0 text-muted">* Required | max 32 chars.</div>
                            @error('name')
                            <div class="alert alert-danger small mt-1">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="col-lg-6">
                            <label for="email" class="small text-muted">Email:</label>
                            <input type="text" name="email" id="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email" aria-label="Email" value="{{ $comment->email ?? old('email') ?? '' }}">
                            <div id="nameHelp" class="form-text small mt-0 text-muted">* Required |  max 32 chars.</div>
                            @error('email')
                            <div class="alert alert-danger small mt-1">{{ $message }}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="mb-2">
                        <label for="message" class="small text-muted">Message:</label>
                        <textarea name="message" id="message" class="form-control @error('message') is-invalid @enderror" rows="3" placeholder="Your Message">{{ $comment->content ?? old('message') ?? '' }}</textarea>
                        <div id="nameHelp" class="form-text mt-0 text-muted">* Required</div>
                        @error('message')
                        <div class="alert alert-danger small mt-1">{{ $message }}</div>
                        @enderror
                    </div>
                    <input type="submit" class="btn btn-secondary my-3" value="Send">
                </form>
            </section>
        </div>
    </div>
@endsection
