@extends('layout.my_monolit')

@section('main')
    <div class="card text-center py-5 mb-4">
        <div class="card-body">
            <h1 class="display-1">Start</h1>
            <p class="lead">Welcome in our website</p>
        </div>
    </div>
    <div class="row lead text-center">
        <div class="col-md-6">
            <div class="mb-2 p-3 start-box">
                <p>Laravel: Debug Bar</p>
            </div>
            <div class="mb-2 p-3 start-box">
                <p>Laravel: Telescope</p>
            </div>
            <div class="mb-2 p-3 start-box">
                <p>Laravel: API</p>
            </div>
        </div>
        <div class="col-md-6">
            <div class="mb-2 p-3 start-box">
                <p>Laravel: Browser tests</p>
            </div>
            <div class="mb-2 p-3 start-box">
                <p>Laravel: Translations (nav & aside)</p>
            </div>
        </div>
    </div>
    <div class="row my-4 text-muted">
        <div class="col">
            <ol class="small text-mited">
                <li>Jak pisać testowalny kod?</li>
                <li>Jak oddzielać Frameworki od Domeny? Wstrzykiwanie serwisów?</li>
                <li>Jak utrzymywać najnowsze wersje Frameworków?</li>
            </ol>
        </div>
    </div>
    {{ config('DEFAULT_IMAGE') }}
    <img src="{{ config('my_monolit.defaultImage') }}" alt="image">
    {{--    {{ dd(Config::get('languages')) }}--}}
@endsection
