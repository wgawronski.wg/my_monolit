@extends('layout.my_monolit')

@section('title', 'Users')

@section('main')
    @include('layout.my_monolit.site_elements.flash_message')
    <section id="order">
        <div class="card pt-5 mb-4">
            <div class="card-body">
                <div class="mb-5">
                    <h1 class="display-1 text-center">Order</h1>
                    <p class="lead text-center">Let's finish it</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <table class="table table-striped table-sm small">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Order ID</th>
                        <th scope="col">User ID</th>
                        <th scope="col">Email</th>
                        <th scope="col">quantity</th>
                        <th scope="col">Price</th>
                        <th scope="col">Created</th>
                        <th scope="col">Actions</th>
                        <th>Items</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($orders as $order)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $order->id }}</td>
                            <td>{{ $order->user_id }}</td>
                            <td>{{ $order->user->email }}</td>
                            <td>{{ $order->quantity }}</td>
                            <td>{{ $order->price }}</td>
                            <td>{{ $order->created_at }}</td>
                            <td><a href="{{ route('web.orders.show', ['order'=> $order->id]) }}">Show</a></td>
                            <td><ul>
                            @foreach($order->products as $product)
                                <li>{{$product->name}}</li>
                            @endforeach
                                </ul></td>

                        </tr>
                    @empty
                        <div class="alert alert-info text-center">No  orders!</div>
                    @endforelse

                    </tbody>
                </table>
            </div>
        </div>
    </section>
@endsection
