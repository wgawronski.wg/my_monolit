@extends('layout.my_monolit')

@section('title', 'Users')

@section('main')
    @include('layout.my_monolit.site_elements.flash_message')
    <section id="order">
        <div class="card pt-5 mb-4">
            <div class="card-body">
                <div class="mb-5">
                    <h1 class="display-1 text-center">Order nr {{ $order->id }} </h1>
                    <p class="lead text-center">Let's check</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <ul>
                    <li>orderID {{ $order->id }} | userID: {{ $order->user_id }}</li>
                    <li>user name: {{ $order->user->name }}</li>
                    <li>user email: {{ $order->user->email }}</li>
                    <li>Quanity: {{ $order->quantity }}</li>
                    <li>Created_at: {{ $order->created_at }}</li>
                </ul>
                <hr>
                <hr>
                <table class="table table-striped table-sm small">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Product ID</th>
                        <th scope="col">Name</th>
                        <th scope="col">Price</th>
                    </tr>
                    </thead>
                    <tbody>
{{--                    {{ dd($order->products()) }}--}}
                    @forelse($order->products as $product)

                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $product->id }}</td>
                            <td>{{ $product->name }}</td>
                            <td>{{ $product->price }}</td>
                        </tr>

                    @empty
                        <div class="alert alert-info text-center">No  orders!</div>
                    @endforelse

                    </tbody>
                </table>
            </div>
        </div>
    </section>
@endsection
