{{--https://bbbootstrap.com/snippets/ecommerce-shopping-cart-30657531--}}

@extends('layout.my_monolit')

@section('title', 'Products')

@section('css-files')
    <link rel="stylesheet" href="{{ asset('css/cart.css') }}">
@endsection

@section('main')
    @include('layout.my_monolit.site_elements.flash_message')
    <section id="cart">
        <div class="card pt-5 mb-4">
            <div class="card-body">
                <div class="mb-5">
                    <h1 class="display-1 text-center">Shopping Cart</h1>
                    <p class="lead text-center">Let's start shopping</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <form action="{{ route('web.orders.store') }}" method="POST">
                    @csrf
                    @method('POST')
                <div class="cart_container">
                    <div class="cart_title d-flex justify-content-between">
                        <span>Shopping Cart</span>
                        <span class="small text-muted font-weight-light">| {{ $cart->getItems()->count() }} product @if($cart->getItems()->count() > 1)s @endif in your cart</span>
                    </div>
{{--                    <div>@include('layout.my_monolit.site_elements.flash_message')</div>--}}
                    <div class="cart_items">
                        <ul class="cart_list">
                            @forelse($cart->getItems() as $item)
                                <li class="cart_item clearfix mb-1 bg-light">
                                    <div class="cart_item_image"><img src="https://i.imgur.com/qqBRWD5.jpg" alt=""></div>
                                    <div class="cart_item_info d-flex flex-md-row flex-column justify-content-between">
                                        <div class="cart_item_name cart_info_col">
                                            <div class="cart_item_title">Name</div>
                                            <div class="cart_item_text"><a href="{{ route('web.products.show', ['slug' => 1]) }}">{{ $item->getName() }}</a></div>
                                        </div>
                                        <div class="cart_item_color cart_info_col">
                                            <div class="cart_item_title">Color</div>
                                            <div class="cart_item_text small"><span style="background-color:#999999;"></span>Silver</div>
                                        </div>
                                        <div class="cart_item_quantity cart_info_col">
                                            <div class="cart_item_title">Quantity</div>
                                            <div class="cart_item_text">{{ $item->getQuantity() }}</div>
                                        </div>
                                        <div class="cart_item_price cart_info_col">
                                            <div class="cart_item_title">Price</div>
                                            <div class="cart_item_text">{{ $item->getPrice() }} </div>
                                        </div>
                                        <div class="cart_item_total cart_info_col">
                                            <div class="cart_item_title">Total</div>
                                            <div class="cart_item_text">{{ $item->getSum() }} {{ __('shop.price.shortcut') }}</div>
                                        </div>
                                        <div class="cart_item_total cart_info_col">
                                            <div class="cart_item_title">&nbsp;</div>
                                            <div class="cart_item_text small">
{{--                                                <form class="d-inline-block" action="{{ route('web.cart.destroy', ['product' => $item->getProductId() ]) }}" method="post">--}}
{{--                                                    @csrf--}}
{{--                                                    @method('DELETE')--}}
{{--                                                    <button type="submit" class="btn btn-sm btn-outline-danger"><i class="far fa-trash-alt"></i></button>--}}
{{--                                                </form>--}}
{{--                                                <form class="d-inline-block" action="{{ route('web.cart.store', ['product' => $item->getProductId(), 'goBackWithMessage' => true ]) }}" method="post">--}}
{{--                                                    @csrf--}}
{{--                                                    @method('POST')--}}
{{--                                                    <button type="submit" class="btn btn-sm btn-outline-info"><i class="fas fa-plus small"></i></button>--}}
{{--                                                </form>--}}
                                                <a href="" class="btn btn-sm btn-outline-warning"><i class="fas fa-minus"></i></a>
                                            </div>
                                        </div>

                                    </div>
                                </li>
                            @empty
                                <li class="cart_item clearfix pb-0 ">
                                    <div class="alert alert-info text-center lead" role="alert">Empty!</div>
                                </li>
                            @endforelse
                        </ul>
                    </div>
                    <div class="order_total">
                        <div class="order_total_content d-flex justify-content-between">
                            <div class="order_total_title">
                                {{ __('shop.order.total') }}:</div>
                            <div class="order_total_amount">{{ $cart->getSum() }} {{ __('shop.price.shortcut') }}</div>
                        </div>
                    </div>
                    <div class="cart_buttons">
                        <a href="{{ route('web.products.index') }}" class="button cart_button_clear">Continue Shopping</a>
                        @if($cart->hasItems())
                            <input type="submit" class="btn btn-lg btn-outline-info rounded-0" value="Pay">
                        @endif
                        <a href="{{ url()->previous() }}" class="btn btn-lg btn-outline-dark rounded-0">Back</a>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </section>
@endsection
