<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class AddressFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::inRandomOrder()->first()->id,
            'city' => $this->faker->city,
            'zip_code' => $this->faker->numerify('#####'),
            'street' => $this->faker->streetName(),
            'street_no' => $this->faker->numerify('###'),
            'home_no' => $this->faker->numerify('##'),
        ];
    }
}
