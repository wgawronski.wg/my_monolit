<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->id();
            $table->string('name', 32);
            $table->string('email', 32);
            $table->unsignedInteger('product_id');
            $table->unsignedInteger('user_id');
            // $table->foreign('user_id')->references('id')->on('users'); // shorter:
            //  $table->foreignId('user_id')->constrained();
            // https://laravel.com/docs/8.x/migrations#foreign-key-constraints
            $table->string('content');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::enableForeignKeyConstraints();
//        Schema::table('comments', function (Blueprint $table) {
//            $table->dropForeign('user_id');
//        });
        Schema::dropIfExists('comments');
    }
}
