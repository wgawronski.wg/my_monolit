<?php

namespace Database\Seeders;

use App\enums\CategoryName;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $names = CategoryName::NAMES;
        $k = 0;
        $data = [
            [
                'name' => $names[$k],
                'slug' => Str::slug($names[$k]),
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => $names[$k],
                'slug' => Str::slug($names[$k]),
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'name' => $names[$k],
                'slug' => Str::slug($names[$k]),
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ];

        foreach ($names as $k => $v){
            // DB::table('categories')->insert($d);
            DB::table('categories')->insert($this->data($names, $k));
        }
    }

    private function data($names, $k){
        return [
            'name' => $names[$k],
            'slug' => Str::slug($names[$k]),
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
