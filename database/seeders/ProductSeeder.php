<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     *  'name' => Str::random(10),
     */
    public function run()
    {
        $data = [
            'name' => 'Product 1',
            'slug' => 'product-1',
            'description' => 'Product 1 description',
            'price' => '199'
        ];
//        \DB::table('products')->insert($data);

         Product::factory()
             ->count(20)
             ->create();
    }
}
