<?php

use App\Http\Controllers\api\v1\ProductController;
use App\Http\Controllers\api\v1\CartController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('v1')->group(function() {
    // http://127.0.0.1:8000/api/v1/test
    // http://localhost/lara/my_monolit/public/api/v1/test
    // Route::get($uri, $callback);
    // https://laravel.com/docs/8.x/routing
    // Route::redirect('/here', '/there');
    // Route::view('/welcome', 'welcome');
    // Route::get('/users/{id}', [UserController::class, 'show']);
    // Route Model Binding
    // Route::get('/users/{user}', [UserController::class, 'show']);

    Route::get('/test', fn() => 'test');

//    Route::get('products', [ProductController::class, 'index']);
//    Route::post('products', function (){
//       \App\Models\Product::create([
//           'name' => 'product one',
//           'slug' => 'product-one',
//           'description' => 'This is product one',
//           'price' => '1.99'
//       ]) ;
//    });
//    Route::post('products', [ProductController::class, 'store']);
//    Route::get('products/{product}', [ProductController::class, 'show']);

    Route::get('products/pagination/{perPage?}', [ProductController::class, 'pagination'])
        ->name('api.products.pagination');
    // ::resource pod ::get w tej samej ścieżce
    Route::resource('products', ProductController::class, ['as' => 'api']);

    Route::get('cart/index', [CartController::class, 'index'])->name('api.cart.index');

});
