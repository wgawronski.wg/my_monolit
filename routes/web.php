<?php

use App\Http\Controllers\web\LanguageController;
use App\Http\Controllers\web\ContactController;
use App\Http\Controllers\web\DarkThemeController;
use App\Http\Controllers\web\CommentController;
use App\Http\Controllers\web\OrderController;
use App\Http\Controllers\web\ProductController;
use App\Http\Controllers\web\UserController;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\web\CategoryController;
use \App\Http\Controllers\web\CartController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::view('/', 'static_sites.start')->name('start_view');
Route::post('/contact', ContactController::class)->name('web.contact_form');
Route::view('/contact', 'static_sites.contact')->name('contact_view');

Route::redirect('products/comments', '/products/comments/start');
Route::resource('products', ProductController::class, ['as' => 'web', 'except' => '']);
//Route::resource('products', ProductController::class);
Route::resource('products.comments', CommentController::class, ['as' => 'web', ]);
Route::get('products/comments/start', [CommentController::class, 'start'])->name('web.products.start');
Route::get('products/{slug}', [ProductController::class, 'show'])->name('web.products.show');

Route::group([
    // 'prefix' => 'pl', //  do geta
    'as' => 'web.',
    'middleware' => [
        // 'verified',
        // 'auth',
        // 'userLogs'
    ],
    ], function() {
        Route::get('categories/', [CategoryController::class, 'index'])->name('categories.index');
        // Route::get('categories/{category}', [CategoryController::class, 'show'])->name('categories.show');
        Route::get('categories/{slug}', [CategoryController::class, 'show'])->name('categories.show');

        // Route::get('users', [UserController::class, 'index'])->name('users.index');
        // Route::get('users/{user}', [UserController::class, 'edit'])->name('users.edit');
        // Route::delete('users/{user}', [UserController::class, 'destroy'])->name('user.delete');
        Route::resource('users', UserController::class)->only(['index', 'edit', 'update', 'destroy']);

        Route::get('cart/index', [CartController::class, 'index'])->name('cart.index');
        Route::post('cart/{product}/{goBackWithMessage?}', [CartController::class, 'store'])->name('cart.store');
        Route::delete('cart/{product}', [CartController::class, 'destroy'])->name('cart.destroy');

        Route::get('orders', [OrderController::class, 'index'])->name('orders.index');
        Route::post('orders', [OrderController::class, 'store'])->name('orders.store');
        Route::get('orders/{order}', [OrderController::class, 'show'])->name('orders.show');
    }
);

Route::get('/dark-theme', DarkThemeController::class)->name('web.dark_theme');
Route::get('lang/{lang}', [LanguageController::class, 'switchLang'])->name('web.lang_switch');
