<?php declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Product extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'slug', 'description', 'price'];

    public function comments() : HasMany
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * @TODO Refactor zaprogramuj życie - kategorie
     */
    //    public function isSelectedCategory(int $category_id): bool
    //    {
    //        return $this->hasCategory() && $this->category->id = $category_id;
    //    }

    //    public function hasCategory()
    //    {
    //        return !is_null($this->category);
    //    }

    public function category() : BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function orders(): BelongsToMany
    {
        return $this->belongsToMany(Order::class);
    }
}
