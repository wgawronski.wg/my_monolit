<?php

declare(strict_types=1);

namespace App\services\contracts;

interface ImageServiceInterface
{
    /**
     * @param  array $imgParams ['id' => '...', 'table' => '...', 'inputName' => '...']
     * @return string Sfilename
     */
    public static function save(array $imgParams) : string;

    /**
     * @param  int   $imageId
     * @param  array $imgParams  ['id' => '...', 'table' => '...', 'inputName' => '...']
     * @param  array $dimensions ['width' => '...', 'height' => '...', 'quality' => '...'] quality: 1-100
     * @return string Sfilename
     */
    public static function saveAndScaleToDimension(int $imageId, array $imgParams, array $dimensions): string;

    /**
     * @param  int   $imageId
     * @param  array $imgParams
     * @param  array $dimensions
     * @return string $imgName
     */
    public static function saveAndTrimToDimensions(int $imageId, array $imgParams, array $dimensions): string;
    

    /**
     * tylko pliki .jpg, .jpeg
     *
     * @param  array $path       ['table' => '...', 'fileName' => 'id_22.jpg ', 'copiedName' => 'id_22_thumb.jpg']
     * @param  array $dimensions
     * @return string $imgName
     */
    public static function resizeImageToNewFile(int $imageId, array $imgParams, array $newDimensions): string;


    /**
     * @param array $imgParams ['table' => '...', 'fileName' => '...', 'tableColumn' => '...']
     */
    public static function remove(array $imgParams) : void;
}