<?php

declare(strict_types=1);

namespace App\services\woj\ImageService;

use App\services\contracts\ImageServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

/**
 * resize image: https://www.youtube.com/watch?v=cog213Ne_bM
 * crop image:
 */
class ImageService implements ImageServiceInterface
{
    /**
     * @return string $fileName
     * @param  array $imgParams ['id' => '...', 'table' => '...', 'inputName' => '...']
     */
    public static function save(array $imgParams): string
    {
        request()->validate(['image' => 'mimes:jpg,png,jpeg|max:1024']);
        self::arrayKeysExists(['id', 'table', 'inputName'], $imgParams);

        $imageId = $imgParams['id'];
        $table = $imgParams['table'];
        $inputName = $imgParams['inputName'];
        $file = request()->file($inputName);
        $fileName = 'id_'.$imageId.'.'.$file->getClientOriginalExtension();

        Storage::disk('public')->putFileAs(
            'img/upload/'.$table,
            $file,
            $fileName
        );

        return $fileName;
    }

    /**
     * Save image and Scale to width or height with ratio*
     * ratio: it can be portrait or landscape
     *
     * @param array $imgParams  ['id' => '...', 'table' => '...', 'inputName' => '...']
     * @param array $dimensions ['width' => '...', 'height' => '...', 'quality' => '...'] quality: 1-100
     */
    public static function saveAndScaleToDimension(int $imageId, array $imgParams, array $dimensions): string
    {
        self::arrayKeysExists(['width', 'height', 'quality'], $dimensions);
        self::arrayKeysExists(['table', 'inputName'], $imgParams);

        $file = request()->file($imgParams['inputName']);
        $tempLocation = request()->file($imgParams['inputName'])->path();
        $imgSize = getimagesize($tempLocation);
        $width = $imgSize[0];
        $height = $imgSize[1];
        $ratio = $width / $height; // Ratio: STOSUNEK

        // if is portrait use ratio to scale
        if ($width > $height) {
            $newWidth = $dimensions['width'];
            $scale = $newWidth/$width;
            $newHeight = $height * $scale;
        } else {
            $newHeight = $dimensions['height'];
            $scale = $newHeight/$height;
            $newWidth = $width * $scale;
        }

        $source = imagecreatefromjpeg($tempLocation);
        $destination = imagecreatetruecolor($newWidth, $newHeight);

        imagecopyresampled(
            $destination,
            $source,
            0,
            0,
            0,
            0,
            $newWidth,
            $newHeight,
            $width,
            $height
        );

        $imgName = 'id_'.$imageId.'.'.$file->getClientOriginalExtension();
        $storagePath  = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();
        $dest = $storagePath.'/img/upload/'.$imgParams['table'].'/'.$imgName;
        imagejpeg($destination, $dest, $dimensions['quality']);

        imagedestroy($destination);
        imagedestroy($source);
        return $imgName;
    }

    /**
     * @param  int   $imageId
     * @param  array $imgParams
     * @param  array $dimensions
     * @return string $imgName
     * https://www.youtube.com/watch?v=Ij-g4T73RAg
     */
    public static function saveAndTrimToDimensions(int $imageId, array $imgParams, array $dimensions): string
    {
        self::arrayKeysExists(['width', 'height', 'quality'], $dimensions);
        self::arrayKeysExists(['table', 'inputName'], $imgParams);
        $file = request()->file($imgParams['inputName']);

        $tempLocation = request()->file($imgParams['inputName'])->path();
        $currentImage = imagecreatefromjpeg($tempLocation);
        $currentWidth = imagesx($currentImage);
        $currentHeight = imagesy($currentImage);

        $imgName = 'id_'.$imageId.'.'.$file->getClientOriginalExtension();
        $storagePath  = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();
        $destFile = $storagePath.'/img/upload/'.$imgParams['table'].'/'.$imgName;

        // $left = ($currentWidth - $dimensions['width']) / 2;
        // $left = 200;
        $left = 0; // crop left margin
        $top = 0;
        $trimWidth = $dimensions['width'];
        $trimHeight = $dimensions['height'];

        $canvas = imagecreatetruecolor($trimWidth, $trimHeight);
        imagecopy($canvas, $currentImage, 0, 0, $left, $top, $currentWidth, $currentHeight);
        imagejpeg($canvas, $destFile, $dimensions['quality']);

        imagedestroy($canvas);
        imagedestroy($currentImage);

        return $imgName;
    }

    /**
     * @param  int   $imageId
     * @param  array $imgParams  tylko zdjęcia .jpg lub
     *                           .jpeg
     * @param  array $dimensions
     * @return string
     */
    public static function saveScaleAndTrimToDimensions(int $imageId, array $imgParams, array $dimensions): string
    {
        self::arrayKeysExists(['width', 'height', 'quality'], $dimensions);
        self::arrayKeysExists(['table', 'inputName'], $imgParams);

        $file = request()->file($imgParams['inputName']);
        $tempLocation = request()->file($imgParams['inputName'])->path();

        $imgSize = getimagesize($tempLocation);
        $currentWidth = $imgSize[0];
        $currentHeight = $imgSize[1];

        $imgName = 'id_'.$imageId.'.'.$file->getClientOriginalExtension();
        $storagePath  = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();
        $destinationPath = $storagePath.'img/upload/'.$imgParams['table'].'/'.$imgName;

        $expectedWidth = $dimensions['width'];
        $expectedHeight = $dimensions['height'];
        // dd("$currentWidth $expectedWidth | $currentHeight $expectedHeight");

        if ($currentWidth < $expectedWidth || $currentHeight < $expectedHeight) {
            return redirect()->back()->with('error', 'Nie udało zapisać się zdjęcia! Za male zdjęcie.');
        }

        $xScale = round($expectedWidth / $currentWidth, 4);
        $yScale = round($expectedHeight / $currentHeight, 4);
        if ($currentHeight * $xScale >= $expectedHeight) {
            $scale = $xScale;
        } elseif ($currentWidth * $yScale >= $expectedWidth) {
            $scale = $yScale;
        } else {
            $scale = 1;
        }
        $newHeight = round($currentHeight * $scale);
        $newWidth = round($currentWidth * $scale);

        // skaluj obrazek
        $sourceImage = imagecreatefromjpeg($tempLocation);
        $scaledImage = imagecreatetruecolor($newWidth, $newHeight);
        imagecopyresampled(
            $scaledImage,
            $sourceImage,
            0,
            0,
            0,
            0,
            $newWidth,
            $newHeight,
            $currentWidth,
            $currentHeight
        );

        // przytnij przeskalowany obrazek
        $left = 0;
        $top = 0;
        $trimmedImage = imagecreatetruecolor($expectedWidth, $expectedHeight);
        imagecopy($trimmedImage, $scaledImage, 0, 0, $left, $top, $currentWidth, $currentHeight);

        imagejpeg($trimmedImage, $destinationPath, $dimensions['quality']);
        imagedestroy($sourceImage);
        imagedestroy($scaledImage);
        imagedestroy($trimmedImage);
        return $imgName;
    }

    /**
     * tylko pliki .jpg, .jpeg
     *
     * @param array $path       ['table' => '...', 'fileName' => 'id_22.jpg ', 'copiedName' => 'id_22_thumb.jpg']
     * @param array $dimensions
     */
    public static function resizeImageToNewFile(int $imageId, array $imgParams, array $newDimensions): string
    {
        self::arrayKeysExists(['table', 'fileName', 'newFileName'], $imgParams);
        self::arrayKeysExists(['width', 'height', 'quality'], $newDimensions);

        $storagePath  = Storage::disk('public')->getDriver()->getAdapter()->getPathPrefix();
        $currentFilePath = $storagePath.'img/upload/'.$imgParams['table'].'/'.$imgParams['fileName'];
        $extension = pathinfo(storage_path($currentFilePath), PATHINFO_EXTENSION);

        $imgSize = getimagesize($currentFilePath);
        $currentWidth = $imgSize[0];
        $currentHeight = $imgSize[1];
        $thumbName = 'id_'.$imageId.'_'.$imgParams['newFileName'].'.'.$extension;

        $destinationPath = $storagePath.'/img/upload/'.$imgParams['table'].'/'.$thumbName;
        $expectedWidth = $newDimensions['width'];
        $expectedHeight = $newDimensions['height'];

        if ($currentWidth < $expectedWidth || $currentHeight == $expectedHeight) {
            return redirect()->back()->with('error', 'Nie udało zapisać się zdjęcia! Za male zdjęcie.');
        }

        $xScale = round($expectedWidth / $currentWidth, 4);
        $yScale = round($expectedHeight / $currentHeight, 4);
        if ($currentHeight * $xScale >= $expectedHeight) {
            $scale = $xScale;
        } elseif ($currentWidth * $yScale >= $expectedWidth) {
            $scale = $yScale;
        } else {
            $scale = $xScale;
            // $scale = 1;
        }
        $newHeight = round($currentHeight * $scale);
        $newWidth = round($currentWidth * $scale);

        // skaluj obrazek
        $sourceImage = imagecreatefromjpeg($currentFilePath);
        $scaledImage = imagecreatetruecolor($newWidth, $newHeight);
        imagecopyresampled(
            $scaledImage,
            $sourceImage,
            0,
            0,
            0,
            0,
            $newWidth,
            $newHeight,
            $currentWidth,
            $currentHeight
        );

        // przytnij przeskalowany obrazek
        $left = 0;
        $top = 0;
        $trimmedImage = imagecreatetruecolor($expectedWidth, $expectedHeight);
        imagecopy($trimmedImage, $scaledImage, 0, 0, $left, $top, $currentWidth, $currentHeight);

        imagejpeg($trimmedImage, $destinationPath, $newDimensions['quality']);
        imagedestroy($sourceImage);
        imagedestroy($scaledImage);
        imagedestroy($trimmedImage);

        return $thumbName;
    }

    /**
     * @param array $imgParams ['table' => '...', 'fileName' => '...', 'tableColumn' => '...']
     */
    public static function remove(array $imgParams)
    {
        self::arrayKeysExists(['table', 'fileName', 'tableColumn'], $imgParams);
        $table = $imgParams['table'];
        $fileName = $imgParams['fileName'];
        $column = $imgParams['tableColumn'];
        $file = 'img/upload/'.$table.'/'.$fileName;
        Storage::disk('public')->delete($file);
        if($column) {
            DB::table($table)->where('id', request()->id)->update([$column => null]);
        }
    }

    /**
     * @param  array $keys
     * @param  array $arr
     * @return bool
     */
    private static function arrayKeysExists(array $keys, array $arr)
    {
        if (array_diff_key(array_flip($keys), $arr)) {
            return redirect()->back()->with('error', 'Nie prawidłowy klucz w tablicy');
        }
    }
}