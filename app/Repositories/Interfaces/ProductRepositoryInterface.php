<?php declare(strict_types=1);

namespace App\Repositories\Interfaces;

interface ProductRepositoryInterface
{
    public function getAll();

    public function getCheapest();

    public function getExpensivest();

}
