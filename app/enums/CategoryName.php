<?php

declare(strict_types=1);

namespace App\enums;

final class CategoryName
{
    const CATEGORY_1 = 'first category';
    const CATEGORY_2 = 'second category';
    const CATEGORY_3 = 'third category';

    const NAMES = [
        self::CATEGORY_1,
        self::CATEGORY_2,
        self::CATEGORY_3,
    ];
}
