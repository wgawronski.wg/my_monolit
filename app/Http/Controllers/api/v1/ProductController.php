<?php declare(strict_types=1);

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductPostRequest;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

/**
 * https://laravel.com/docs/8.x/controllers
 */
class ProductController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ProductResource::collection(Product::all());
        return Product::paginate(4);
        return \DB::table('products')->paginate(3);
        return Product::all();
    }

    public function pagination(int $perPage = 10)
    {
        $list = Product::paginate($perPage);
        return response()->json($list, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() : Response
    {
        die;
    }

    /**
     * Store a newly created resource in storage.
     * POSTMAN:
     * Headers: application/json
     * Body: form-data
     * Body: name: Product Two
     * Body: slug: product-two
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductPostRequest $request)
    {
        // return Product::create($request->all());
        $product = Product::create($request->validated());
        return new ProductResource($product);
    }

    public function show(Product $product) : ProductResource
    {
        return new ProductResource($product);
        // return $product;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) : Response
    {
        die;
    }

    public function update(ProductPostRequest $request, int $id) : Response
    {
        $validated = $request->validated();
        $product = Product::findOrFail($id);
    }

    public function destroy(int $id) : SymfonyResponse
    {
        $product = Product::findOrFail($id); // 500
        // $product = Product::find($id); // 500
        $product->delete();

        // HTTP Status 204 (No Content) - RESTful API
        return response(null, SymfonyResponse::HTTP_NO_CONTENT);
        return response(null, 204);
    }
}
