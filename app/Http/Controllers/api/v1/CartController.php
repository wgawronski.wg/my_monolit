<?php

namespace App\Http\Controllers\api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\CartResource;
use App\valueObjects\Cart;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CartController extends Controller
{
    public function index(): JsonResponse
    {
        $cart = Session::get('cart', new Cart());
        return response()->json($cart, 200);
        // return CartResource::collection($cart);
    }
}
