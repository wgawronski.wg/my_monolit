<?php declare(strict_types=1);

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DarkThemeController extends Controller
{
    public function __invoke(Request $request)
    {
        if (session()->has('isDark')) {
            session()->put('isDark', !session('isDark'));
        } else {
            //provide an initial value of isDark
            session()->put('isDark', true);
        }

        return redirect()->back();
    }
}
