<?php

declare(strict_types=1);

namespace App\Http\Controllers\web;

use App\valueObjects\Cart;
use App\valueObjects\CartItem;
use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;

/**
 * Czy może w sesji przechowywać tylko IDiki produktów,
 * a w koszyku odptać DB o te produkty?
 */
class CartController extends Controller
{
    public function index(): View
    {
        $cart = Session::get('cart', new Cart());
        return view('cart.index', compact('cart'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function store(Product $product, bool $goBackWithMessage = null): RedirectResponse
    {
        $cart = Session::get('cart', new Cart());
        Session::put('cart', $cart->addItem($product));

        if($goBackWithMessage == true) {
            return redirect()->back()->with('success', 'Dodano element do koszyka.');
        }
        return redirect()->route('web.cart.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int                      $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function destroy(Product $product): RedirectResponse
    {
        try{
            $cart = Session::get('cart', new Cart());
            Session::put('cart', $cart->removeItem($product));
            return redirect()->back()->with('success', 'Rekord usunięto');
        } catch (\Exception $e)
        {
            return redirect()->back()->with('error', 'Wystąpił błąd, nie można usunąć pozycjiusunięto');
        }
    }

    private function incrementQuantity()
    {
        $this->quantity += 1;
    }
}
