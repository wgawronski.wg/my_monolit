<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductPostRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Models\Comment;
use App\Models\Product;
use App\Repositories\ProductRepository;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

class ProductController extends Controller
{
    private ProductRepository $productRepository;

    /**
     * @TODO  wstrzykiwać interfejs
     * np.:
     * https://www.twilio.com/blog/repository-pattern-in-laravel-application
     * @param ProductRepository $productRepository
     */
    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function index(): View
    {
        $products = Product::paginate(9);
        // $products =  $this->productRepository->getAll();
        // $products = Product::all();
        return view('products.index', compact('products'));
    }

    public function create(): View
    {
        return view('products.create');
    }

    public function store(ProductPostRequest $request) : View
    {
        $product = Product::create($request->validated());
        return view('products.show', compact('product'))->with('success', 'Success');;
    }

    public function show(string $slug) : View
    {
        $product = Product::with(['comments', 'category'])
            ->where('slug', $slug)
            ->firstOrFail();
         return view('products.show', compact('product', ));
    }

    public function edit(string $slug): View
    {
        $product = Product::where('slug', $slug)->firstOrFail();
        return view('products.edit', compact('product'));
    }

    public function update(ProductUpdateRequest $request, $id)
    {
        $product = Product::findOrFail($id);
        $product->update($request->validated());
        return redirect()
            ->route('products.show', ['slug' => $request->validated()['slug']])
            ->with('success', 'Product updated');
    }

    public function destroy(Product $product): RedirectResponse
    {
        $product->delete();
        return redirect()->route('web.products.index')->with('success', 'Product deleted!');
    }
}
