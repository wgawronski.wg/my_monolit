<?php

declare(strict_types=1);

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Address;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\View\View;

class UserController extends Controller
{
    public function index(): View
    {
        // $users = User::with('address')->get();
        $users = User::all();
        return view('users.index', compact('users'));
    }

    public function edit(User $user): View
    {
        // $user = $user->load('address');
        return view('users.edit', compact('user'));
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        $addressValidated = $request->validated()['address'];
        if($user->hasAddress()) {
            $address = $user->address;
            $address->fill($addressValidated);
        }
        else{
            $address = new Address($addressValidated);
        }
        $user->address()->save($address);
        return redirect()->back()->with('success', 'Dane zaktualizowano.');
    }

    public function destroy(User $user)
    {
        dd($user);

        try {

        } catch (\Exception $e){

        }
    }
}
