<?php declare(strict_types=1);

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\View\View;

class CategoryController extends Controller
{
    public function index(): View
    {
        $categories = Category::all();
        return view('categories.index', compact('categories'));
    }

    // public function show(Category $category): View
    public function show(string $slug): View
    {
        // Collection of.. users
        // $users = User::with('profile')->get();
        // or:
        // $users = User::all();
        // $users->load('profile');
        // $category->load('products');
        $category = Category::where('slug', $slug)->with('products')->firstOrFail();
        return view('categories.show', compact('category'));
    }
}
