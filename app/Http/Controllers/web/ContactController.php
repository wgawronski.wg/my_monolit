<?php declare(strict_types=1);

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function __invoke(Request $request) : RedirectResponse
    {
        $request->validate(
            [
            'name' => 'required|max:32',
            'email' => 'required|email',
            'message' => 'required|string',
            ]
        );

        return back()->with('success', 'Send a message');
    }
}
