<?php declare(strict_types=1);

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Http\Requests\CommentStoreRequest;
use App\Http\Requests\CommentUpsertRequest;
use App\Models\Comment;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\View\View;

class CommentController extends Controller
{
    public function start(): View
    {
        $products = Product::all('name', 'slug');
        return view('products.comments.start', compact('products'));
    }

    public function index(Request $request): View
    {
        $products = Product::all('name', 'slug');
        $product = Product::where('slug', $request->product)
            ->with('comments')
            ->firstOrFail();
        return view('products.comments.index', compact('product', 'products'));
    }

    public function create(Request $request): View
    {
        $product = Product::where('slug', $request->product)->first();
        $products = Product::all('name', 'slug');
        return view('products.comments.create', compact('products', 'product'));
    }

    public function show(Request $request): View
    {
        $products = Product::all('name', 'slug');
        $product = Product::where('slug', $request->product)
            ->with('comments')
            ->firstOrFail();
        $comment = Comment::findOrFail($request->comment);
        return view('products.comments.show', compact('product', 'products', 'comment'));
    }

    public function store(CommentStoreRequest $request, Product $product)
    {
        $validated = $request->validated();
        $validated['product_id'] =$product->id;
        $product_id = $product->id;
        dd($validated);
        Comment::created($validated);
    }

    public function edit(Product $product, Comment $comment): View
    {
        return view('products.comments.edit', compact('comment', 'product'));
    }

    public function update(CommentUpsertRequest $request, $id)
    {
        $comment = Comment::findOrFail($id);
        $comment->update($request->validated());
        return redirect()->back()->with('success', __('crud.actions.update'));
    }

    public function destroy(Product $product, Comment $comment)
    {
        $comment->delete();
        return redirect()->back()->with('success', __('crud.actions.delete'));
        // return redirect()->back()->with('success', 'Element usunięto');
    }

    public function destroyAndRedirectToIndex(Product $product, Comment $comment)
    {
        $comment->delete();
        return redirect()->route('web.products.index')->with('success', 'Element usunięto');
    }
}
