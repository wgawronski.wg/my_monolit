<?php

declare(strict_types=1);

namespace App\Http\Controllers\web;

use App\Models\Order;
use App\valueObjects\Cart;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\View\View;


/**
 * many to many
 * wiele produktów do wielu zamówień
 * one to many
 * jeden uzytkownik wiele zamówien
 */
class OrderController extends Controller
{
    public function index(): View
    {
        $orders = Order::all();
        // tylko dla zalogowanych
        // $orders = Order::where('id', Auth::id())->get();
        return view('orders.index', compact('orders'));
    }

    public function show(Order $order)
    {
        return view('orders.show', compact('order'));
    }

    public function store(): RedirectResponse
    {
        /**
         * TODO transakcje (rollback): a.,. b., c.
         */
        $cart = Session::get('cart', new Cart());
        if($cart->hasItems()) {
            // a.
            $order = new Order();
            $order->quantity = $cart->getQuantity();
            $order->price = $cart->getSum();
            // @TODO logowanie uzytkowników - skladanie zamowienai tylko dla zalogowanych
            $order->user_id = Auth::id() ?? 2;
            $order->save();

            // b.
            $productIds = $cart->getItems()->map(
                function ($item) {
                    return ['product_id' => $item->getProductId()];
                }
            );
            $order->products()->attach($productIds);

            // c.
            Session::put('cart', new Cart());
            return redirect()->route('web.orders.index')->with('success', __('orders.order.realized'));
        }
        else{
            return back();
        }
    }

}
