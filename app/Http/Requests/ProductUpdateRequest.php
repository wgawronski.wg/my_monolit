<?php declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class ProductUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => 'required|min:5|max:64',
            'slug' => 'required|unique:products,'.$this->id,
            'description' => 'required|string',
            'price' => 'required',
        ];
    }

    protected function prepareForValidation() : void
    {
        $this->merge(
            [
                'slug' => Str::slug($this->name),
            ]
        );
    }
}
