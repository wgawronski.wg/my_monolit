<?php declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class ProductPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize() : bool
    {
        return true;
    }

    public function rules() : array
    {
        return [
            'name' => 'required|max:64',
            'slug' => 'required|unique:products',
            'description' => 'required|string',
            'price' => 'required',
        ];
    }

    public function messages() : array
    {
        return [
            'name.required' => 'Some product name is required',
            'slug.unique' => 'This name is already taken',
        ];
    }

    public function attributes() : array
    {
        return [
            'description' => 'Description',
        ];
    }

    protected function prepareForValidation() : void
    {
        $this->merge(
            [
            'slug' => Str::slug($this->name),
            ]
        );
    }
}
