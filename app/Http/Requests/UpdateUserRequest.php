<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        //            'name' => 'required|string|max:255',
        //            // 'email' => 'required|email|unique:users,email'.$this->user->id,
        //            'email' => [
        //                'required','email',
        //                Rule::unique('users', 'email')->ignore($this->user->id),
        //            ],
            'address.city' =>'required|max:255',
            'address.street' =>'required|max:255',
            'address.zip_code' =>'required|max:6',
            'address.street_no' =>'required|max:5',
            'address.home_no' =>'required|max:5',
        ];
    }
}
