<?php

declare(strict_types=1);

namespace App\valueObjects;

use App\valueObjects\contracts\CartInterface;
use Closure;
use Illuminate\Support\Collection;
use App\Models\Product;

/**
 * DTO - obiekty niemutowalne w ciagu swojego życia (a przecież są settery)
 * DTO - brak logiki biznesowej: ++totalQuantity, ++totalSum
 * to "głupie obiekty do przechowywania wartości i przekazania dalej
 * > konieczny refactor
 *
 * DTO:          has data; no logic; no ID
 * ValueObject:  has data; has logic; no ID
 * Entity:       has data; has logic, has ID
 *
 * VO, DTO: zmiana wartości: nowy obiek na bazie starego
 */
class Cart implements CartInterface
{
    /**
     * @var Collection
     */
    private Collection $items;
    //    private array $items = [];
    //    private float $totalSum = 0;
    //    private float $totalQuantity = 0;
    /**
     * @param Collection|null $items
     */
    public function __construct(Collection $items = null)
    {
        $this->items = $items ?? Collection::empty();
    }

    public function addItem(Product $product): Cart
    {
        $items = $this->items;
        $item = $items->first($this->isProductIdSameAsItemProduct($product));

        if(!is_null($item)) {
            // gdy obiekt jest znaleziony suwamy znaleziony obiekt z listy i podbijamy wattość
            // tworząc nowy obietk
            // i nowy obiekt dodajemy do lity, aby uniknąć duplikacji
            $items = $this->removeItemsFromCollection($items, $product);
            $newItem = $item->addQuantity($product);
        }
        else{
            $newItem = new CartItem($product);
        }

        $items->add($newItem);
        return new Cart($items);
    }

    public function removeItem(Product $product): Cart
    {
        $items = $this->removeItemsFromCollection($this->items, $product);
        return new Cart($items);
    }

    public function getItems(): Collection
    {
        return $this->items;
    }

    public function hasItems(): bool
    {
        return $this->items->isNotEmpty();
    }

    public function getQuantity(): int
    {
        return $this->items->sum(
            function ($item) {
                return $item->getQuantity();
            }
        );
    }

    public function getSum(): float
    {
        return $this->items->sum(
            function ($item) {
                return $item->getQuantity();
            }
        );
    }

    /**
     * @param  Product $product
     * @return \Closure
     */
    public function isProductIdSameAsItemProduct(Product $product): Closure
    {
        return function ($item) use ($product) {
            return $product->id == $item->getProductId();
        };
    }

    /**
     * @param  Product $product
     * @return Collection
     */
    private function removeItemsFromCollection(Collection $items, Product $product): Collection
    {
        return $items->reject($this->isProductIdSameAsItemProduct($product));
    }
}
