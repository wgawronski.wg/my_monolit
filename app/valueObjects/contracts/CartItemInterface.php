<?php

namespace App\valueObjects\contracts;

use App\Models\Product;
use App\valueObjects\CartItem;

interface CartItemInterface
{
    public function getProductId(): int;

    public function getName(): string;

    public function getPrice(): float;

    public function getQuantity(): int;

    public function getSum(): float;

    public function addQuantity(Product $product): CartItem;

}
