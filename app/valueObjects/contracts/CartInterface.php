<?php

declare(strict_types=1);

namespace App\valueObjects\contracts;

use App\Models\Product;
use App\valueObjects\Cart;
use Illuminate\Support\Collection;

interface CartInterface
{
    public function addItem(Product $product) : Cart;

    public function removeItem(Product $product) : Cart;

    public function getItems() : Collection;

    public function hasItems() : bool;

    public function getQuantity() : int;

    public function getSum() : float;

}
