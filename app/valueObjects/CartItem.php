<?php

declare(strict_types=1);

namespace App\valueObjects;

use App\Models\Product;
use App\valueObjects\contracts\CartItemInterface;

class CartItem implements CartItemInterface
{
    private int $productId;
    private string $name;
    private float $price;
    private int $quantity = 0;

    public function __construct(Product $product, int $quantity = 1)
    {
        $this->productId = $product->id;
        $this->name = $product->name;
        $this->price = (float) $product->price;
        $this->quantity = $quantity;
    }

    /**
     * @return int
     */
    public function getProductId(): int
    {
        return $this->productId;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function getSum(): float
    {
        return $this->price*$this->quantity;
    }

    public function addQuantity(Product $product): CartItem
    {
        return new CartItem($product, ++$this->quantity);
    }
}

