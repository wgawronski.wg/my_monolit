<?php

namespace App\Console\Commands;

use App\Models\Product;
use Illuminate\Console\Command;

class DeleteAllProductsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'products:delete-all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete all prpducts from user table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        \DB::table('products')->truncate();
        $this->info('Delete all products!');
        return Command::SUCCESS;
    }
}
