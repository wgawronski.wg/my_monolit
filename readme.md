

<<<<<<< Updated upstream
#### My Moonolit

1. [Założenia](#założenia)
2. [Stack](#stack)
3. [Postawienie projektu](#postawienie projektu)
---
4. Docker: FE, BE, DB; konfiguracja, np. .env
5. MySQL/SQLite (ew. w migracjach proste ify które rozwiązują konflikty)
6. Serwer: a) apache (xampp), b) Laravelowy 
7. [Testy przeglądarkowe](#Laravel Dusk)
8. Lara: Wykorzystanie Fabryk i Seederów
9. Lara: cieńkie kontrollery
10. Lara: tłumaczenia
11. Lara: Eloquent lub Query-Builder
12. Lara: Paasport, Telescope, Dusk, DebugBar
13. BE - Monolit; wystawia RESTowe api (7 metod)
14. Dokumentacja BE: Swagger
15. Statyczna analiza kodu
---

### Założenia
- aplikacja dostępna przez www
- aplikacja dostępna przez RestApi
- zarządzanie aplikacją przez konsolę: Artisan  
- Dostępny z apacza i natywnego servera
- Over-engineering mile widziany
---
### Stack  

php -v ::: PHP 8.0.13  
laravel -V ::: Laravel Installer 4.2.9  
node -v ::: v16.13.1  
npm -v ::: 8.1.2  
composer -V ::: Composer version 2.1.14

---

### Postawienie projektu
sudo apt install php-zip \ 
php-curl;  
sudo apt-get install php-sqlite3  

git clone;  
composer install;  
npm install;  
npm init;  
cp .env.example .env;  
php artisan key:generate;  
sudo chmod 777 storage/logs/laravel.log   
sudo chmod 777 -R storage/framework/sessions/  
? composer require doctrine/dbal;  
? php service apache2 restart;  

> SQLite  
> touch database/database.sqlite  
> sudo chmod 777 database/database.sqlite; 
> .env:  
> DB_CONNECTION=sqlite  

> MySQL  
> .env:  
> DB_CONNECTION=mysql  
> DB_HOST=127.0.0.1  
> DB_PORT=3306  
> DB_DATABASE=my_monolit  
> DB_USERNAME=root  
> DB_PASSWORD=  

php artisan migrate --seed;  
php artisah serve;  
http://127.0.0.1:8000  
npm run watch;  
http://127.0.0.1:3000  
npm run dev;
---

### Frontend
https://laravel.com/docs/8.x/mix  

koszyk
https://bbbootstrap.com/snippets/ecommerce-shopping-cart-30657531

---

php artisan make:controller web/CommentController --resource;  
php artisan make:model Comment -m;  

### Cache
php artisan optimize:clear;  
php artisan cache:clear;  
php artisan debugbar:clear;  
php artisan route:clear;
php artisan route:cache;  


#### rebuild database during development  
php artisan migrate --seed;
php artisan migrate:rollback;
php artisan migrate:fresh --seed;  
php artisan migrate:refresh --seed;

// migrate:refresh first rolls back all migrations, then runs them again, effectively re-creating the database. 
// You can pass a --step= param to roll back a certain amount.  

// fresh pomija down() methods
// fresh: drops all the tables, then runs your migrations
// specific migration  
! php artisan migrate:refresh --path=database/migrations/2022_01_01_191831_create_addresses_table.php  
php artisan migrate:fresh --path=database/migrations/2022_01_01_191831_create_addresses_table.php  


---
## Paczki
- Laravel migrations generator
- Laravel Permisionns
- laravel permissions
- swaggerAPI vs Scribe  
scribe; tworzenie na podstawie adnotajcji w kodzie   
gotowego dokumentu  
- debugbar  
https://github.com/barryvdh/laravel-debugbar  
composer require barryvdh/laravel-debugbar --dev  
- telescope // how debug invisble query? API?  
https://laravel.com/docs/8.x/telescope
useage: http://localhost:3000/telescope/requests
- Pretty routes
```
php artisan route:pretty
php artisan route:pretty --except-path=horizon --method=POST --reverse
php artisan route:pretty --only-path=app --method=POST --reverse
php artisan route:pretty --only-name=app --method=POST 
php artisan route:pretty --only-name=app --method=POST --group=path --reverse-group
php artisan route:pretty --only-name=app,horizon,debugbar --except-path=foo,bar --group=name --reverse
```
- laravel backup
> .zip  
> cron  
> ci/cd - b. przed odpaleniem migracji  
> lokalnie, z poziomu aplikacji  

- laravel/enum
composer require bensampo/laravel-enum  
php artisan make:enum UserType  

-  php_codesniffer  
composer require --dev squizlabs/php_codesniffer  
./vendor/bin/phpcs ./app  
./vendor/bin/phpcbf ./app  
./vendor/bin/phpcbf ./tests/  

### Laravel Dusk
https://laravel.com/docs/8.x/dusk  
APP_URL=http://127.0.0.1:8000
php artisan dusk:make LoginTest  
php artisan dusk
php artisan dusk --help  
php artisan dusk --filter test_contact_site  
php artisan dusk tests/Browser/StaticSitesTest.php


### Config
key - value  
/config/my_constans.php -> **config()** vs env()    
.env -> SOME_CONST="some value"  
php artisan config:cache 
php artisan config:clear  

### Unit tests
./vendor/bin/phpunit --version  
./vendor/bin/phpunit tests/Unit/ExampleTest.php
php artisan test;  

php artisan make:test CartTest --unit
php artisan test --filter CartTest
php artisan test --filter CartTest::test_get_product_id

### Language switcher 
https://5balloons.info/localization-laravel-multi-language-language-switcher/  

### Service providers
php artisan make:provider ViewServiceProvider

```
# APP_URL=http://my_monolit.test
APP_URL=http://127.0.0.1:8000
```

### Emails, Event and Listeners I
https://mailtrap.io/  
https://mailtrap.io/blog/send-email-in-laravel/  

### Observers, Event and Listeners II
np. Podczas usuwania module chcemy usunąć Wszystkie komentarzae  
a nie chcemy mieć czyste modele  
Events - Obserwers  
https://laravel.com/docs/8.x/eloquent#observers  
php artisan make:observer ProductObserver --model=Product  

---

### Products
https://www.youtube.com/watch?v=MT-GJQIY3EU  

php artisan make:model Product --migration;  
php artisan migrate;  
php artisan make:controller api/v1/ProductController --resource;  
php artisan make:resource ProductResource;  
php artisan make:request ProductPostRequest;  

php artisan route:list;  
php artisan route:list --name=products;  
php artisan route:list --method=post;  

php artisan make:seeder ProductSeeder;  
php artisan make:factory ProductFactory; 

php artisan db:seed;
php artisan tinker;  
DB::select('show tables'); // MySQL  
DB::select('SELECT NAME FROM sqlite_master WHERE type="table"');  
\App\Models\Product::all(); // Eloquent  

DB::table('products')->get(); // QueryBuilder  
$product = DB::table('products')->where('id', 1)->get();  
$product = \App\Models\Product::find(1);  
$product->delete();  
\App\Models\Product::all();  

https://laravel.com/docs/8.x/controllers;  

php artisan make:command DeleteAllProductsCommand;  

php artisan config:cache;  
php artisan config:clear;  

Bind the interface and the implementation  
php artisan make:provider RepositoryServiceProvider;  

### Architektura:
https://stawarczyk.pl/2021/01/architektura-heksagonalna/
