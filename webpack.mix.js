const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    // .js('resources/js/app.js', 'public/js')
    // .postCss('resources/css/app.css', 'public/css', [
    //     //
    // ]);
    .postCss('resources/css/cart.css', 'public/css')
    .sass('resources/sass/my_monolit.scss', 'public/css')
    .sass('resources/sass/my_dashboard.scss', 'public/css')
    .browserSync({
        proxy: "127.0.0.1:8000",
        files: [
            // 'public/css/app.css',
            // 'public/js/app.js',
            'app/**/*',
            'routes/**/*',
            'resources/sass/**/*',
            'resources/sass/*',
            'resources/views/**/*',
            'resources/lang/**/*'
        ]
    })
    .sourceMaps();
