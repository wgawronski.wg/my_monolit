https://clockworkjava.pl/2020/12/obiekt-domenowy-dto-dao/

Obiekt domenowy to serce całej domeny. Jest to główna klasa, której to obiekty reprezentują główną rzecz/idee obsługiwaną przez daną domenę.  

Czy  wyznaczenie obiektów domenowych jest dość proste?

E-commerce
- koszyk
- magazyn
- dostawa
- platnosc
- ?

Obiekty domenowe są zapisywane w bazie danych (albo persystowane w inny sposób) i zmiany w ich wartościach są audytowane (logowane, zapisywane by było wiadomo kiedy i co się zmieniło). Generalnie stanowią serce naszego programu.


DTO (Data Transfer Object)
Drugim typem obiektów jest DTO, czyli Data Transfer Object. Są to obiekty, które są tylko zbiorem danych (pól) i posiadają tylko zestaw getterów. Nie posiadają one żadnej logiki biznesowej.

Służą one do przesłania danych poza część aplikacji odpowiedzialną za logikę biznesową do na przykład front-endu. Jeśli ktoś chcę, by jego kod był bardzo “zamknięty” to posuwa się krok dalej i z domeny nie “wychodzą” obiekty domenowe, a okrojone DTO właśnie.

Obiekty typu DTO są dość “lekkie” i nie są w żaden sposób przechowywane w bazie danych oraz audytowane, więc możemy klas takich obiektów tworzyć wiele. Obiekt domenowy Reservation może mieć kilka DTO – każdy tworzony na potrzebny jednej metody, jednego “endpointa” w systemie.

Jeśli nasza aplikacja udostępnia dane o rezerwacjach poprzez interfejs REST to zamiast wysyłać na świat cały obiekt domenowy Reservation, który może posiadać bardzo dużo informacji (np. dane adresowe osoby aktualnie rezerwującej dany pokój) i narażać się na przechwycenie tych danych lepiej stworzyć obiekt DTO. Posiada on część danych niezbędnych w danym kontekście i jest tworzony na podstawie obiektu domenowego. To właśnie DTO jest zwracane przez REST.




