<?php

namespace Tests\Unit;

use App\Models\Product;
use App\valueObjects\Cart;
//use PHPUnit\Framework\TestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;


class CartItemTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_add_item_is_added_to_cart()
    {
        $cart = new Cart();
        $product = Product::first();
//        dd($product);
        $cart->addItem($product);

        $this->assertTrue(true);
    }
}
