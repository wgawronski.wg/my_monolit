<?php

namespace Tests\Feature;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProductsTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_a_product_can_be_added_to_store()
    {
        $response = $this->post('/products', [
            'id' => 1,
            'name' => 'My name',
            'slug' => 'my-slug',
            'description' => 'My description',
            'price' => 111,
        ]);

//        $response->assertOk();
        $this->assertCount(1, Product::all());

        $response->assertStatus(200);
    }

    public function test_i_cant_see_a_product_one()
    {
        $response = $this->get('/products/1');

        $response->assertStatus(404);
    }
}
