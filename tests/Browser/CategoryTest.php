<?php

namespace Tests\Browser;

use App\enums\CategoryName;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class CategoryTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function test_aside_categories()
    {
        $this->browse(
            function (Browser $browser) {
                $categories = CategoryName::NAMES;
                foreach ($categories as $c){
                    $browser->visit('/')
                        ->assertSee('My Monolit')
                        ->assertSeeIn('aside', $c)
                        ->assertSeeIn('aside > .categories > .list-group', $c);
                }
            }
        );
    }

    public function test_index_categories()
    {
        $this->browse(
            function (Browser $browser) {
                $categories = CategoryName::NAMES;
                foreach ($categories as $c){
                    $browser->visit('/categories')
                        ->assertSee($c);
                }
                if(count($categories)) {
                    $browser->visit('/categories/1')
                        ->assertSee('first category');
                }
            }
        );
    }

    public function test_click_first_category()
    {
        $this->browse(
            function (Browser $browser) {
                $categories = CategoryName::NAMES;
                // var_dump(count($categories));die;
                if(count($categories)) {
                    $browser->visit('/categories')
                        ->assertSeeIn('main', 'Categories')
                        ->assertSeeIn('main', 'See our categories');
                }
            }
        );
    }
}
