<?php declare(strict_types=1);

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class ContactTest extends DuskTestCase
{
    public function test_contatct_site() : void
    {
        $this->browse(
            function (Browser $browser) {
                $browser->visit('/contact')
                    ->assertSee('Contact us. Write an email or fill in the form.')
                    ->assertSee('Contact')
                    ->assertSee('shop@contact.us');
            }
        );
    }

    public function test_validate_contatct_form() : void
    {
        $this->browse(
            function (Browser $browser) {
                $browser->visit('/')
                    ->clickLink('Contact')
                    ->press('Send')
                    ->assertSee('The name field is required.')
                    ->assertSee('The email field is required.')
                    ->assertSee('The message field is required.');
            }
        );
    }

    public function test_send_contatc_form() : void
    {
        $this->browse(
            function (Browser $browser) {
                $browser->visit('/')
                    ->clickLink('Contact')
                    ->type('email', 'rolladen@test.com')
                    ->type('name', 'Test user')
                    ->type('message', 'Test message.')
                    ->press('Send')
                    ->assertSee('Send a message');
            }
        );
    }


}
