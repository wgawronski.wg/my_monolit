<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class StaticSitesTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function test_start_site()
    {
        $this->browse(
            function (Browser $browser) {
                $browser->visit('/')
                    ->assertSee('Laravel')
                    ->assertSee('My Monolit')
                    ->assertSee('Welcome in our website')
                    ;
            }
        );
    }

    public function test_contact_site()
    {
        $this->browse(
            function (Browser $browser) {
                $browser->visit('/')
                    ->assertSee('Start');
                $browser->clickLink('Contact')
                    ->assertSee('Contact us. Write an email or fill in the form.');
            }
        );
    }

    public function test_contact_email()
    {
        $this->browse(
            function (Browser $browser) {
                $browser->visit('/contact')
                    ->assertSee('shop@contact.us')
                    ->seeLink('shop@contact.us');
            }
        );
    }
}
